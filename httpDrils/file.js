const fs = require('fs')
const path = require('path')
const uuid = require('uuid')
const http = require('http')

function getHTMLFile(response) {
    const filePath = path.join(__dirname, 'index.html')
    response.writeHead(200, { 'Content-Type': 'text/html' });
    fs.readFile(filePath, (err, data) => {
        if (err) {
            response.writeHead(500, { 'Content-Type': 'text/plain' });
            response.write('500 Internal Server Error');
        } else {
            response.write(data)
        }
        response.end();
    });
}

function getJsonFile(response) {
    const filePath = path.join(__dirname, 'app.json')
    response.writeHead(200, { 'Content-Type': "application/json" });
    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            response.writeHead(500, { 'Content-Type': 'text/plain' });
            response.end('500 Internal Server Error');
        } else {
            response.write(data)
        }
        response.end();

    });
}
function getUniqueId(response) {
    const randomUUID4 = uuid.v4();
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify({ uuid: randomUUID4 }));
    response.end()
}

function getStatusCode(url) {
    const parts = url.split("/");
    if (parts.length === 3 && !isNaN(parts[2])) {
        const code = (parts[2]);
        if (Object.keys(http.STATUS_CODES).includes(code)) {
            return code;
        }
    }
    return null;
}

function getStatusFromUrl(url, response) {
    const statusCode = getStatusCode(url);
    if (statusCode !== null) {
        response.statusCode = statusCode;
        response.setHeader("Content-Type", "text/plain");
        response.end(`Status code ${statusCode} - ${http.STATUS_CODES[statusCode]}`);
    } else {
        response.statusCode = 400;
        response.setHeader("Content-Type", "text/plain");
        response.end("Invalid status code");
    }
}
function getDelayedResponseFromUrl(url, response) {
    const delayInSeconds = url.split("/")[2];
    if (!isNaN(delayInSeconds)) {
        setTimeout(() => {
            response.writeHead(200, { "Content-Type": "text/plain" });
            response.end(`Response delayed by ${delayInSeconds} seconds`);
        }, delayInSeconds * 1000);
    } else {
        response.writeHead(400, { "Content-Type": "text/plain" });
        response.write("Invalid delay time specified");
    }
}
module.exports = { getHTMLFile, getJsonFile, getUniqueId, getStatusFromUrl, getDelayedResponseFromUrl }
