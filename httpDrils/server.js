const http = require('http');
const { getHTMLFile, getJsonFile, getUniqueId, getStatusFromUrl, getDelayedResponseFromUrl } = require('./file');

const PORT = 4000

const server = http.createServer((request, response) => {
    if (request.url === '/html') {
        getHTMLFile(response)
    }
    else if (request.url === '/json') {
        getJsonFile(response)
    }
    else if (request.url === '/uuid') {
        getUniqueId(response)

    }
    else if (request.url.startsWith("/status/")) {
        getStatusFromUrl(request.url, response)
    }
    else if (request.url.indexOf("/delay/") > -1) {
        getDelayedResponseFromUrl(request.url, response)
    }
    else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end('404 Not Found');
    }
});

server.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
